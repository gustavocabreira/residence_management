<?php

use Illuminate\Http\Request;

Route::prefix('v1')->group(function() {
    Route::prefix('residences')->group(function () {
        Route::post('/list', 'ResidenceController@index');
        Route::post('/create', 'ResidenceController@store');
        Route::post('/update', 'ResidenceController@update');
        Route::post('/delete', 'ResidenceController@delete');
    });

    Route::prefix('categories')->group(function() {
        Route::post('/list', 'CategoryController@list');
        Route::post('/create', 'CategoryController@store');
    });
});