<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residences', function(Blueprint $table) {
            $table->increments('residence_id');
            $table->text('residence_name');
            $table->text('residence_address');
            $table->text('residence_number');
            $table->text('residence_district');
            $table->integer('residence_city');
            $table->integer('residence_zip_code');
            $table->text('residence_reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
