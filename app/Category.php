<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'cat_id_residence', 'cat_id_user', 'cat_description', 'cat_type',
        'cat_is_active'
    ];

    protected $table = "categories";
    protected $primaryKey = "cat_id";
}
