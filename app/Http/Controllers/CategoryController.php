<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function list(Request $request) {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'residence_id' => 'required'
        ]);

        if ($validator->fails()) {    
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $categories = Category::where('cat_id_user', $request->input('user_id'))
                            ->where('cat_id_residence', $request->input('residence_id'))
                            ->get();
        
        return response()->json(['success' => true, 'data' => $categories], 200);
    }

    public function store(Request $request) {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'residence_id' => 'required',
            'description' => 'required|max:190',
            'type' => 'required'
        ]);

        if ($validator->fails()) {    
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $category = new Category;
        $category->cat_id_residence = $request->input('residence_id');
        $category->cat_id_user = $request->input('user_id');
        $category->cat_description = $request->input('description');
        $category->cat_type = $request->input('type');

        if($category->save()) {
            return response()->json(['success' => true], 200);
        } else {
            return responde()->json(['success' => false], 500);
        }

    }
}
