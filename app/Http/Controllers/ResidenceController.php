<?php

namespace App\Http\Controllers;

use App\Residence;
use App\ResidencesUsers;
use Illuminate\Http\Request;

class ResidenceController extends Controller
{
    /**
     * Essa função irá listar as residências
     */
    public function index() {
        $residences = Residence::all();
        return response()->json($residences);
    }

    /**
     * Essa função armazenará na tabela residences
     */
    public function store(Request $request) {
        // dd($request->all());
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:190',
            'user_id' => 'required',
            'address' => 'required|max:190',
            'number' => 'required|max:10',
            'district' => 'required|max:50',
            'city_id' => 'required',
            'zip_code' => 'required|max:8',
            'reference' => 'nullable|max:150'
        ]);
        
        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $residence = new Residence;

        $residence->residence_name = $request->input('name');
        $residence->residence_address = $request->input('address');
        $residence->residence_number = $request->input('number');
        $residence->residence_district = $request->input('district');
        $residence->residence_city = $request->input('city_id');
        $residence->residence_zip_code = $request->input('zip_code');
        if($request->input('reference') != null) $residence->residence_reference = $request->input('reference');

        $response = [
            'success' => true,
            'message' => null
        ];

        // Caso a residence for salvo com sucesso
        if($residence->save()) {
            // Recupero o id inserido
            $id = $residence->id;

            // Insiro a residences_users
            $resp = self::storeResidencesUsers(['residence_id' => $id, 'user_id' => $request->input('user_id')]);

            // Se a residences_users criado com sucesso
            if($resp) { 
                return response()->json($response, 200);
            } else {
                $response['success'] = false;
                $response['message'] = ["Cannot save users residences"];

                return response()->json($response, 500);
            }
        } else {
            $response['success'] = false;
            $response['errors'] = ["Cannot save residence"];

            return response()->json($response, 500);
        }
    }

    /**
     * Essa função armazenará na tabela residences_users
     */
    public function storeResidencesUsers($request) {
        $data = new ResidencesUsers;

        $data->ru_id_user = $request['user_id'];
        $data->ru_id_residence = $request['residence_id'];
        $data->ru_access_level = $access_level ?? 1;

        return $data->save();
    }

    /**
     * Essa função atualizará a tabela residences
     */
    public function update(Request $request) {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:190',
            'user_id' => 'required',
            'address' => 'required|max:190',
            'number' => 'required|max:10',
            'district' => 'required|max:50',
            'city_id' => 'required',
            'zip_code' => 'required|max:8',
            'reference' => 'nullable|max:150',
            'residence_id' => 'required'
        ]);

        // then, if it fails, return the error messages in JSON format
        if ($validator->fails()) {    
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        // Procurando a residence com o id informado
        $residence = Residence::find($request->input('residence_id'));

        // Se estiver vazio, retorno erro
        if(empty($residence)) return response()->json(['success' => false, 'errors' => ['Residence not found']], 404);

        $residence->residence_name = $request->input('name');
        $residence->residence_address = $request->input('address');
        $residence->residence_number = $request->input('number');
        $residence->residence_district = $request->input('district');
        $residence->residence_city = $request->input('city_id');
        $residence->residence_zip_code = $request->input('zip_code');
        if($request->input('reference') != null) $residence->residence_reference = $request->input('reference');

        $response = [
            'success' => true,
            'message' => null
        ];

        // Caso a residence for salvo com sucesso
        if($residence->save()) {
            return response()->json($response, 200);
        } else {
            $response['success'] = false;
            $response['errors'] = ["Cannot update residence"];

            return response()->json($response, 500);
        }
    }

    /**
     * Essa função irá deletar a residence e todos os residences_users
     */
    public function delete(Request $request) {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'residence_id' => 'required'
        ]);

        // Buscando a residence
        $residence = Residence::find($request->input('residence_id'));

        // Caso não exista
        if(empty($residence)) return response()->json(['success' => false, 'errors' => ['Residence not found']], 404);

        // Caso deletado com sucesso
        if($residence->delete()) {
            // Caso residences_users deletado com sucesso
            if($this->deleteResidencesUsers($request->input('residence_id'))) {
                return response()->json(['success' => true, 'message' => null], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'Cannot delete residences users'], 500);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Cannot delete data']);
        }
    }

    /**
     * Essa função irá deletar todos os residences_users da residence selecionada
     */
    public function deleteResidencesUsers($residence_id) {
        return ResidencesUsers::where('ru_id_residence', $residence_id)->delete();
    }
}
