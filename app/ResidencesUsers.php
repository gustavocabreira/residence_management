<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResidencesUsers extends Model
{
    protected $fillable = [
        'ru_id_user', 'ru_id_residence', 'ru_access_level'
    ];
    
    protected $table = 'residences_users';
}
