<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residence extends Model
{
    protected $fillable = [
        'residence_name', 'residence_address', 'residence_number',
        'residence_district', 'residence_city', 'residence_zip_code',
        'conference'
    ];
    
    protected $table = 'residences';

    protected $primaryKey = 'residence_id';
}
